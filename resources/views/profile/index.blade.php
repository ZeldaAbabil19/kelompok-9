@extends('layout.master')

@section('title')
    User Id {{ Auth::user()->name }}
@endsection

@section('content')
@if ($profile != null)
<div>
    <h2>Edit Profile</h2>    
    <h5>Email : {{$profile->user->email}}</h5>
    <h5>Nama  : {{$profile->user->name}}</h5>

        <form action="/profile/{{$profile->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="umur">umur</label>
                <input type="text" class="form-control" name="umur" id="umur" value="{{$profile->umur}}" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">bio</label>
                <input type="text" class="form-control" name="bio" value="{{$profile->bio}}" id="bio" placeholder="Masukkan Bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" value="{{$profile->alamat}}" name="alamat" id="alamat" placeholder="Masukkan Alamat">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
  </div>

@else

<div>
    <span class="badges badge-primary">edit profil</span>
        <form action="/profile" method="POST">
            @csrf
            <div class="form-group">
                <label for="umur">umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">bio</label>
                <input type="text" class="form-control" name="bio"id="bio" placeholder="Masukkan Bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat">
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
  </div>
@endif
@endsection
